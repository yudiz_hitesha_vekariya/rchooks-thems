import "./App.css";
import Main from "./components/Main";
import ThemeContext, { themes } from "./components/context/Theme-context";
import { useState } from "react";

function App() {
  const [theme, setTheme] = useState(themes.dark);

  const toggleTheme = () =>
    theme === themes.dark ? setTheme(themes.light) : setTheme(themes.dark);
  return (
    <div className="App">
      <ThemeContext.Provider value={theme}>
        <button className="btn" onClick={toggleTheme}>
          CHANGE THEME
        </button>
        <Main />
      </ThemeContext.Provider>
    </div>
  );
}

export default App;
