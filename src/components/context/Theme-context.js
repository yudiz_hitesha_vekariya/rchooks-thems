import React from "react";

export const themes = {
  dark: {
    color: "black",
    background: "orange",
  },
  light: {
    color: "white",
    background: "black",
  },
};

const ThemeContext = React.createContext(themes.dark);

export default ThemeContext;
