import React, { useState, useReducer, useContext } from "react";
import ThemeContext from "./context/Theme-context";

const reducerfunction = (state, action) => {
  switch (action.type) {
    case "increment":
      return {
        ...state,
        count: state.count + 1,
      };
    case "decrement":
      return {
        ...state,
        count: state.count - 1,
      };
    case "initCount":
      return {
        ...state,
        count: action.payload,
      };
    default:
      return state;
  }
};

const initialstate = {
  count: 0,
};
const Main = () => {
  const theme = useContext(ThemeContext);
  const [input, setInput] = useState(0);
  const [state, dispatch] = useReducer(reducerfunction, initialstate);
  return (
    <div className="main_div" style={theme}>
      <h1>Magic with Reducer</h1>
      <div>
        <label>start count:</label>

        <br />
        <input
          type="number"
          onChange={(e) => {
            setInput(parseInt(e.target.value));
          }}
          value={input}
        ></input>
        <br></br>
        <br />
        <button
          className="btn1"
          onClick={() => dispatch({ type: "initCount", payload: input })}
        >
          initialize counter
        </button>
      </div>
      <p className="pera">{state.count}</p>
      <button className="btn2" onClick={() => dispatch({ type: "increment" })}>
        increment
      </button>
      <br />
      <br />
      <button className="btn2" onClick={() => dispatch({ type: "decrement" })}>
        decrement
      </button>
    </div>
  );
};

export default Main;
